package com.EAuction.cqrs.core.exceptions;

public class FutureDateException extends RuntimeException {
    public FutureDateException(String message) {
        super(message);
    }
}
