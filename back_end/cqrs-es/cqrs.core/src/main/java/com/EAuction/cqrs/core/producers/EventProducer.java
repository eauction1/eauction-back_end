package com.EAuction.cqrs.core.producers;

import com.EAuction.cqrs.core.events.BaseEvent;

public interface EventProducer {
    void produce(String topic, BaseEvent event);
}
