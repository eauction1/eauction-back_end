package com.EAuction.cqrs.core.exceptions;

public class PastBidEndDateException extends RuntimeException {
    public PastBidEndDateException(String message) {
        super(message);
    }
}
