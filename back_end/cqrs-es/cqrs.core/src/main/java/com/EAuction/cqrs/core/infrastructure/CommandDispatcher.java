package com.EAuction.cqrs.core.infrastructure;

import com.EAuction.cqrs.core.commands.BaseCommand;
import com.EAuction.cqrs.core.commands.CommandHandlerMethod;

public interface CommandDispatcher {
    <T extends BaseCommand> void registerHandler(Class<T> type, CommandHandlerMethod<T> handler);
    void send(BaseCommand command);
}
