package com.EAuction.cqrs.core.exceptions;

public class AtLeastOneBidPresentException extends RuntimeException {
    public AtLeastOneBidPresentException(String message) {
        super(message);
    }
}
