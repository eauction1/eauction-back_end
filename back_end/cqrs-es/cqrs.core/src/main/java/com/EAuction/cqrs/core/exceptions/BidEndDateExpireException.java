package com.EAuction.cqrs.core.exceptions;

public class BidEndDateExpireException extends RuntimeException {
    public BidEndDateExpireException(String message) {
        super(message);
    }
}
