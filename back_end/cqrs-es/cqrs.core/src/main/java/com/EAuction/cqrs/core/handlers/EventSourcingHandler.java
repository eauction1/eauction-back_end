package com.EAuction.cqrs.core.handlers;

import com.EAuction.cqrs.core.domain.AggregateRoot;

public interface EventSourcingHandler<T>  {
    void save(AggregateRoot aggregate);
    T getById(String id);
}
