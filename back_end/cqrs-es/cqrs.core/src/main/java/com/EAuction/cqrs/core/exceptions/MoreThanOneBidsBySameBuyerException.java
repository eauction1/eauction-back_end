package com.EAuction.cqrs.core.exceptions;

public class MoreThanOneBidsBySameBuyerException extends RuntimeException {
    public MoreThanOneBidsBySameBuyerException(String message) {
        super(message);
    }
}
