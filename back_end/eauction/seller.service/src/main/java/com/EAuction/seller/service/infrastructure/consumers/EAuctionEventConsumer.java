package com.EAuction.seller.service.infrastructure.consumers;

import com.EAuction.common.service.events.BidPlacedEvent;
import com.EAuction.common.service.events.BidUpdatedEvent;
import com.EAuction.seller.service.infrastructure.handlers.EventHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Service;

@Service
public class EAuctionEventConsumer implements EventConsumer{
    @Autowired
    private EventHandler eventHandler;

    @KafkaListener(topics = "BidPlacedEvent", groupId = "${spring.kafka.consumer.group-id}")
    @Override
    public void consume(BidPlacedEvent event, Acknowledgment ack) {
        this.eventHandler.on(event);
        ack.acknowledge();
    }

    @KafkaListener(topics = "BidUpdatedEvent", groupId = "${spring.kafka.consumer.group-id}")
    @Override
    public void consume(BidUpdatedEvent event, Acknowledgment ack) {
        this.eventHandler.on(event);
        ack.acknowledge();
    }
}
