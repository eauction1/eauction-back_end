package com.EAuction.seller.service.domain;

import com.EAuction.cqrs.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class BuyersBid extends BaseEntity {
    @Id
    private String id;
    private String firstName;
    private String lastName;
    private String address;
    private String city;
    private String state;
    private int pin;
    private String phone;
    private String email;
    private String productId;
    private double bidAmount;
}
