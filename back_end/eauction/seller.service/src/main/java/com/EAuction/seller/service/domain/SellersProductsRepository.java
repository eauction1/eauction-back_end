package com.EAuction.seller.service.domain;

import org.springframework.data.repository.CrudRepository;

public interface SellersProductsRepository extends CrudRepository<SellersProducts, String> {
}
