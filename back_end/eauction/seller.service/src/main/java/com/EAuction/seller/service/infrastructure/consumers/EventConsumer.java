package com.EAuction.seller.service.infrastructure.consumers;

import com.EAuction.common.service.events.BidPlacedEvent;
import com.EAuction.common.service.events.BidUpdatedEvent;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.messaging.handler.annotation.Payload;

public interface EventConsumer {
    void consume(@Payload BidPlacedEvent event, Acknowledgment ack);
    void consume(@Payload BidUpdatedEvent event, Acknowledgment ack);
}
