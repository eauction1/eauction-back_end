package com.EAuction.seller.service.domain;

import com.EAuction.common.service.dto.ProductCategory;
import com.EAuction.cqrs.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class SellersProducts extends BaseEntity {
    @Id
    private String productId;
    @NotBlank(message = "Product name should be present, Please check!")
    @Size(min = 5, max = 30, message = "Product name size must be between 5 and 30")
    private String productName;
    private String shortDescription;
    private String detailDescription;
    @Enumerated(EnumType.STRING)
    private ProductCategory productCategory;
    private double startPrice;
    @JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING)
    @Column(name = "bid_end_date")
    private String bidEndDate;
    @NotBlank(message = "First Name should be present, Please check!")
    @Size(min = 5, max = 30, message = "First name size must be between 5 and 30")
    private String firstName;
    @NotBlank(message = "Last Name should be present, Please check!")
    @Size(min = 3, max = 25, message = "Last name size must be between 3 and 25")
    private String lastName;
    private String address;
    private String city;
    private String state;
    private int pin;
    @NotNull(message = "Phone number should be present, Please check!")
    @Pattern(regexp = "\\d{10}", message = "Phone number is not in format, Please add 10 digit numeric Phone number!")
    private String phone;
    @NotNull(message = "Email should be present, Please check!")
    @Email(message = "Email is not in format, Please add single @")
    private String email;
}
