package com.EAuction.seller.service.api.dto;

import com.EAuction.common.service.dto.BaseResponse;
import com.EAuction.seller.service.domain.BuyersBid;
import com.EAuction.seller.service.domain.SellersProducts;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class ProductBidsLookupResponse extends BaseResponse {
    private SellersProducts sellersProducts;
    private List<BuyersBid> buyersBids;

    public ProductBidsLookupResponse(String message) {
        super(message);
    }
}
