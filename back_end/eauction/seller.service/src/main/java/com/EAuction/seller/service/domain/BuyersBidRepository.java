package com.EAuction.seller.service.domain;

import com.EAuction.cqrs.core.domain.BaseEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BuyersBidRepository extends CrudRepository<BuyersBid, String> {

    List<BaseEntity> findByProductId(String productId);
    List<BuyersBid> findByEmail(String email);
}
