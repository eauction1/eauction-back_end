package com.EAuction.seller.service.infrastructure.handlers;

import com.EAuction.common.service.events.BidPlacedEvent;
import com.EAuction.common.service.events.BidUpdatedEvent;

public interface EventHandler {
    void on(BidPlacedEvent event);
    void on(BidUpdatedEvent event);
}
