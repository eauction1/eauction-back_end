package com.EAuction.seller.service.infrastructure.services;

import com.EAuction.cqrs.core.domain.BaseEntity;
import com.EAuction.cqrs.core.exceptions.AtLeastOneBidPresentException;
import com.EAuction.cqrs.core.exceptions.BidEndDateExpireException;
import com.EAuction.cqrs.core.exceptions.FutureDateException;
import com.EAuction.cqrs.core.infrastructure.QueryDispatcher;
import com.EAuction.seller.service.api.queries.FindBidsByProductIdQuery;
import com.EAuction.seller.service.domain.BuyersBid;
import com.EAuction.seller.service.domain.BuyersBidRepository;
import com.EAuction.seller.service.domain.SellersProducts;
import com.EAuction.seller.service.domain.SellersProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Service
public class SellerService {
    @Autowired
    private QueryDispatcher queryDispatcher;

    @Autowired
    private SellersProductsRepository sellersProductsRepository;

    @Autowired
    private BuyersBidRepository buyersBidRepository;

    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    LocalDateTime now = LocalDateTime.now();
    public SellersProducts addProduct(SellersProducts sellersProducts) {
        SellersProducts addedSellersProducts = sellersProductsRepository.save(sellersProducts);
        return addedSellersProducts;
    }

    public String deleteProduct(String productId) {
        Optional<SellersProducts> sellersProducts =sellersProductsRepository.findById(productId);
        if(sellersProducts.get().getBidEndDate().compareTo(dtf.format(now)) < 0) {
            throw new BidEndDateExpireException("Product is tried to be deleted after bid end date!");
        }
        List<BuyersBid> buyersBids = queryDispatcher.send(new FindBidsByProductIdQuery(productId));
        if(buyersBids != null && buyersBids.size() > 0) {
            throw new AtLeastOneBidPresentException("Product is tried to be deleted when at least one bid is already placed!");
        }

        sellersProductsRepository.deleteById(productId);
        return "Product with product id "+productId+" deleted successfully!";
    }

    public SellersProducts getProduct(String productId) {
        Optional<SellersProducts> sellersProducts =sellersProductsRepository.findById(productId);
        return sellersProducts.get();
    }

    public List<BuyersBid> getEmail(String email) {
        List<BuyersBid> buyersbidsList = buyersBidRepository.findByEmail(email);
        return buyersbidsList;
    }
}
