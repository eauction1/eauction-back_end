package com.EAuction.seller.service.api.queries;

import com.EAuction.cqrs.core.queries.BaseQuery;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FindBidsByProductIdQuery extends BaseQuery {
    private String productId;
}
