package com.EAuction.seller.service.api.controllers;

import com.EAuction.common.service.dto.BaseResponse;
import com.EAuction.cqrs.core.infrastructure.QueryDispatcher;
import com.EAuction.seller.service.api.dto.ProductBidsLookupResponse;
import com.EAuction.seller.service.api.queries.FindBidsByProductIdQuery;
import com.EAuction.seller.service.domain.BuyersBid;
import com.EAuction.seller.service.domain.SellersProducts;
import com.EAuction.seller.service.infrastructure.services.SellerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.text.MessageFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
@RequestMapping(path = "/e-auction/api/v1/seller")
@Api(value = "ProductBidsController, Controller that deals with getting bids for product!")
public class ProductBidsController {
    private final Logger logger = Logger.getLogger(ProductBidsController.class.getName());

    @Autowired
    private QueryDispatcher queryDispatcher;

    @Autowired
    private SellerService sellerService;

    @GetMapping("/show-bids/{productId}")
    @ApiOperation(value = "Get bids for product", response = ProductBidsLookupResponse.class, tags = "getBidsByProductId")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Got the bids for product successfully!"),
            @ApiResponse(code = 500, message = "Error while processing request to get bids for product!")})
    public ResponseEntity<ProductBidsLookupResponse> getBidsByProductId(@PathVariable(value = "productId") String productId) {
        try {
            logger.log(Level.INFO, "Inside getBidsByProductId method of ProductBidsController!");

            SellersProducts sellersProducts = sellerService.getProduct(productId);
            List<BuyersBid> buyersBids = queryDispatcher.send(new FindBidsByProductIdQuery(productId));
            var response = ProductBidsLookupResponse.builder()
                    .sellersProducts(sellersProducts)
                    .buyersBids(buyersBids)
                    .message(MessageFormat.format("Successfully return product information with {0} bid(s)!", buyersBids.size()))
                    .build();
            logger.log(Level.INFO, MessageFormat.format("Successfully return product information with bid(s). - {0}", response));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            var safeErrorMessage = "Failed to get product information with bids!";
            logger.log(Level.SEVERE, safeErrorMessage, e);
            return new ResponseEntity<>(new ProductBidsLookupResponse(safeErrorMessage), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
