package com.EAuction.seller.service.api.queries;

import com.EAuction.cqrs.core.domain.BaseEntity;
import com.EAuction.seller.service.domain.BuyersBidRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EAuctionQueryHandler implements QueryHandler{
    @Autowired
    private BuyersBidRepository buyersBidRepository;

    @Override
    public List<BaseEntity> handle(FindBidsByProductIdQuery query) {
        List<BaseEntity> buyersbidList = buyersBidRepository.findByProductId(query.getProductId());
        return buyersbidList;
    }
}
