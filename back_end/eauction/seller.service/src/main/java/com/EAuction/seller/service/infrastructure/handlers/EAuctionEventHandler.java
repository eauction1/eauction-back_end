package com.EAuction.seller.service.infrastructure.handlers;

import com.EAuction.common.service.events.BidPlacedEvent;
import com.EAuction.common.service.events.BidUpdatedEvent;
import com.EAuction.seller.service.domain.BuyersBid;
import com.EAuction.seller.service.domain.BuyersBidRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EAuctionEventHandler implements EventHandler{
    @Autowired
    private BuyersBidRepository buyersBidRepository;

    @Override
    public void on(BidPlacedEvent event) {
        var buyerBid = BuyersBid.builder()
                .id(event.getId())
                .firstName(event.getFirstName())
                .lastName(event.getLastName())
                .address(event.getAddress())
                .city(event.getCity())
                .state(event.getState())
                .pin(event.getPin())
                .phone(event.getPhone())
                .email(event.getEmail())
                .productId(event.getProductId())
                .bidAmount(event.getBidAmount())
                .build();
        buyersBidRepository.save(buyerBid);
    }


    @Override
    public void on(BidUpdatedEvent event) {
        var buyerBid = buyersBidRepository.findById(event.getId());
        if(buyerBid.isEmpty()) {
            return;
        }
        var bidAmount = buyerBid.get().getBidAmount();
        var updatedBidAmount = event.getUpdatedBidAmount();
        buyerBid.get().setBidAmount(updatedBidAmount);
        buyersBidRepository.save(buyerBid.get());
    }
}
