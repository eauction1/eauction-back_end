package com.EAuction.seller.service.api.queries;

import com.EAuction.cqrs.core.domain.BaseEntity;

import java.util.List;

public interface QueryHandler {
    List<BaseEntity> handle(FindBidsByProductIdQuery query);
}
