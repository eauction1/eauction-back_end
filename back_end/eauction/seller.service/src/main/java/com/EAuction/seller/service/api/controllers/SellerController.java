package com.EAuction.seller.service.api.controllers;

import com.EAuction.common.service.dto.BaseResponse;
import com.EAuction.cqrs.core.exceptions.FutureDateException;
import com.EAuction.seller.service.domain.BuyersBid;
import com.EAuction.seller.service.domain.SellersProducts;
import com.EAuction.seller.service.infrastructure.services.SellerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.Valid;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
@RequestMapping(path = "/e-auction/api/v1/seller")
@Api(value = "SellerController, Controller that deals with adding and deleting product!")
public class SellerController {
    private final Logger logger = Logger.getLogger(SellerController.class.getName());
    @Autowired
    private SellerService sellerService;

    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    LocalDateTime now = LocalDateTime.now();

    @PostMapping("/add-product")
    @ApiOperation(value = "Add product", response = SellersProducts.class, tags = "addProduct")
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Product added successfully!"),
            @ApiResponse(code = 400, message = "Client made a bad request!"),
            @ApiResponse(code = 500, message = "Error while processing request to add product!")})
    public ResponseEntity addProduct(@Valid @RequestBody SellersProducts sellersProducts) {

        try {
            logger.log(Level.INFO, "Inside addProduct method of SellerController!");
            if(sellersProducts.getBidEndDate().compareTo(dtf.format(now)) < 0) {
                throw new FutureDateException("Bid end date should be future date!");
            }
            SellersProducts addedSellersProducts = sellerService.addProduct(sellersProducts);
            logger.log(Level.INFO, MessageFormat.format("Successfully processed request to add product! - (0).", addedSellersProducts));
            return new ResponseEntity<>(addedSellersProducts, HttpStatus.CREATED);
            } catch (IllegalStateException e) {
            logger.log(Level.WARNING, MessageFormat.format("Client made a bad request - {0}.", e.toString()));
            return new ResponseEntity<>(new BaseResponse(e.toString()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            var safeErrorMessage = MessageFormat.format("Error while processing request to add product - {0}.", e.toString());
            logger.log(Level.SEVERE, safeErrorMessage, e);
            return new ResponseEntity<>(new BaseResponse(safeErrorMessage), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{productId}")
    @ApiOperation(value = "Delete product", response = SellersProducts.class, tags = "deleteProduct")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Product deleted successfully!"),
            @ApiResponse(code = 400, message = "Client made a bad request!"),
            @ApiResponse(code = 500, message = "Error while processing request to delete product!")})
    public ResponseEntity deleteProduct(@PathVariable(value = "productId") String productId) {

        try {
            logger.log(Level.INFO, "Inside deleteProduct method of SellerController!");
            String message = sellerService.deleteProduct(productId);
            logger.log(Level.INFO, MessageFormat.format("Successfully processed request to delete product! - (0).", productId));
            return new ResponseEntity<>(message, HttpStatus.OK);
        } catch (IllegalStateException e) {
            logger.log(Level.WARNING, MessageFormat.format("Client made a bad request - {0}.", e.toString()));
            return new ResponseEntity<>(new BaseResponse(e.toString()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            var safeErrorMessage = MessageFormat.format("Error while processing request to delete product - {0}.", e.toString());
            logger.log(Level.SEVERE, safeErrorMessage, e);
            return new ResponseEntity<>(new BaseResponse(safeErrorMessage), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/getProduct/{productId}")
    public ResponseEntity getProduct(@PathVariable(value = "productId") String productId) {

        try {
            logger.log(Level.INFO, "Inside getProduct method of SellerController!");
            SellersProducts sellersProducts = sellerService.getProduct(productId);
            logger.log(Level.INFO, MessageFormat.format("Successfully processed request to get product for id - {0}.", productId));
            return new ResponseEntity<>(sellersProducts, HttpStatus.OK);
        } catch (IllegalStateException e) {
            logger.log(Level.WARNING, MessageFormat.format("Client made a bad request - {0}.", e.toString()));
            return new ResponseEntity<>(new BaseResponse(e.toString()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            var safeErrorMessage = MessageFormat.format("Error while processing request to get product for id - {0}.", productId);
            logger.log(Level.SEVERE, safeErrorMessage, e);
            return new ResponseEntity<>(new BaseResponse(safeErrorMessage), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/getProductsBids/{email}")
    public ResponseEntity getProductsBids(@PathVariable(value = "email") String email) {

        try {
            logger.log(Level.INFO, "Inside getProductsBids method of SellerController!");
            List<BuyersBid> buyersBidsList = sellerService.getEmail(email);
            logger.log(Level.INFO, MessageFormat.format("Successfully processed request to get products bids for email - {0}.", email));
            return new ResponseEntity<>(buyersBidsList, HttpStatus.OK);
        } catch (IllegalStateException e) {
            logger.log(Level.WARNING, MessageFormat.format("Client made a bad request - {0}.", e.toString()));
            return new ResponseEntity<>(new BaseResponse(e.toString()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            var safeErrorMessage = MessageFormat.format("Error while processing request to get products bids for email - {0}.", email);
            logger.log(Level.SEVERE, safeErrorMessage, e);
            return new ResponseEntity<>(new BaseResponse(safeErrorMessage), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
