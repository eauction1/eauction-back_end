package com.EAuction.seller.service.controllers;

import com.EAuction.common.service.dto.ProductCategory;
import com.EAuction.cqrs.core.infrastructure.QueryDispatcher;
import com.EAuction.seller.service.api.controllers.SellerController;
import com.EAuction.seller.service.api.queries.QueryHandler;
import com.EAuction.seller.service.domain.SellersProducts;
import com.EAuction.seller.service.infrastructure.services.SellerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(SellerController.class)
@AutoConfigureMockMvc
public class SellerControllerUnitTest {
        @Autowired
        MockMvc mockMvc;

        ObjectMapper objectMapper = new ObjectMapper();

        @MockBean
        QueryDispatcher queryDispatcher;

        @MockBean
        QueryHandler queryHandler;

        @MockBean
        SellerService sellerService;

        @Test
        void addProduct() throws Exception {

                // given
                // Always create new SellersProducts here to test this method
                SellersProducts sellersProducts = SellersProducts.builder()
                        .productId("S3")
                        .productName("Air compass")
                        .shortDescription("International symposium at Canada")
                        .detailDescription("Fixed on top of the other so it could still move and feel the wind")
                        .productCategory(ProductCategory.SCULPTOR)
                        .startPrice(5000.0)
                        .bidEndDate("2022-12-06")
                        .firstName("Nicholas")
                        .lastName("Runolfsdottir")
                        .address("Ellsworth Summit")
                        .city("Aliyaview")
                        .state("UK")
                        .pin(12255)
                        .phone("1115556666")
                        .email("Nicholas@gmail.com")
                        .build();

                String json = objectMapper.writeValueAsString(sellersProducts);
//                doReturn(sellersProductsResponse).when(buyerClient).getProduct(isA(String.class));

                // when
                mockMvc.perform(post("/e-auction/api/v1/seller/add-product")
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON))
                        .andExpect(status().isCreated());

                // then

        }

        @Test
        void addProduct_4XX() throws Exception {
                // Phone number is not in format, Please add 10 digit numeric Phone number

                // given

                SellersProducts sellersProducts = SellersProducts.builder()
                        .productId("S3")
                        .productName("Air compass")
                        .shortDescription("International symposium at Canada")
                        .detailDescription("Fixed on top of the other so it could still move and feel the wind")
                        .productCategory(ProductCategory.SCULPTOR)
                        .startPrice(5000.0)
                        .bidEndDate("2022-12-06")
                        .firstName("Nicholas")
                        .lastName("Runolfsdottir")
                        .address("Ellsworth Summit")
                        .city("Aliyaview")
                        .state("UK")
                        .pin(12255)
                        .phone("1115") // Phone number is not in format, Please add 10 digit numeric Phone number
                        .email("Nicholas@gmail.com")
                        .build();

                String json = objectMapper.writeValueAsString(sellersProducts);
//                doReturn(sellersProductsResponse).when(buyerClient).getProduct(isA(String.class));

                // when
                mockMvc.perform(post("/e-auction/api/v1/seller/add-product")
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON))
                        .andExpect(status().is4xxClientError());

                // then
        }

        @Test
        void addProduct_5XX() throws Exception {
                // Bid end date should be future date

                // given

                SellersProducts sellersProducts = SellersProducts.builder()
                        .productId("S3")
                        .productName("Air compass")
                        .shortDescription("International symposium at Canada")
                        .detailDescription("Fixed on top of the other so it could still move and feel the wind")
                        .productCategory(ProductCategory.SCULPTOR)
                        .startPrice(5000.0)
                        .bidEndDate("2022-10-06") // Bid end date should be future date
                        .firstName("Nicholas")
                        .lastName("Runolfsdottir")
                        .address("Ellsworth Summit")
                        .city("Aliyaview")
                        .state("UK")
                        .pin(12255)
                        .phone("1115556666")
                        .email("Nicholas@gmail.com")
                        .build();

                doReturn(sellersProducts).when(sellerService).addProduct(isA(SellersProducts.class));

                String json = objectMapper.writeValueAsString(sellersProducts);
//                doReturn(sellersProductsResponse).when(buyerClient).getProduct(isA(String.class));

                // when
                mockMvc.perform(post("/e-auction/api/v1/seller/add-product")
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON))
                        .andExpect(status().is5xxServerError());

                // then
        }

        @Test
        void deleteProduct() throws Exception {

                // given

                String resourceUrl = "/e-auction/api/v1/seller/delete/O1";


                // when
                mockMvc.perform(delete(resourceUrl))
                        .andExpect(status().isOk());

                // then

        }

        @Test
        void deleteProduct_4XX() throws Exception {

                // Product is tried to be deleted after bid end date

                // given

                String resourceUrl = "/e-auction/api/v1/seller/deleteR2";

                // when

                mockMvc.perform(delete(resourceUrl))
                        .andExpect(status().is4xxClientError());

                // then

        }




}
