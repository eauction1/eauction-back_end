package com.EAuction.seller.service.controllers;

import com.EAuction.common.service.dto.ProductCategory;
import com.EAuction.cqrs.core.infrastructure.QueryDispatcher;
import com.EAuction.seller.service.api.controllers.ProductBidsController;
import com.EAuction.seller.service.api.queries.FindBidsByProductIdQuery;
import com.EAuction.seller.service.api.queries.QueryHandler;
import com.EAuction.seller.service.domain.BuyersBid;
import com.EAuction.seller.service.domain.SellersProducts;
import com.EAuction.seller.service.infrastructure.services.SellerService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ProductBidsController.class)
@AutoConfigureMockMvc
public class ProductBidsControllerUnitTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    QueryDispatcher queryDispatcher;

    @MockBean
    QueryHandler queryHandler;

    @MockBean
    SellerService sellerService;


    @Test
    void getBidsByProductId() throws Exception {

        // given

        SellersProducts sellersProducts = SellersProducts.builder()
                .productId("S2")
                .productName("Gian Lorenzo Bernini")
                .shortDescription("Ecstasy of Saint Teresa, 1647–52")
                .detailDescription("Acknowledged as an originator of the High Roman Baroque style")
                .productCategory(ProductCategory.SCULPTOR)
                .startPrice(3500.0)
                .bidEndDate("2022-12-04")
                .firstName("Clementine")
                .lastName("Bauch")
                .address("Douglas Extension")
                .city("McKenziehaven")
                .state("Canada")
                .pin(12244)
                .phone("1115556665")
                .email("Clementine@gmail.com")
                .build();

        BuyersBid buyersBid = BuyersBid.builder()
                .id("47345cd4-971a-4477-a88d-65763e9ae285")
                .firstName("Dennis")
                .lastName("Schulist")
                .address("Norberto Crossing")
                .city("Christy")
                .state("Spain")
                .pin(12212)
                .phone("1112223333")
                .email("Dennis@gmail.com")
                .productId("S2")
                .bidAmount(3000)
                .build();

        List<BuyersBid> buyersBidList = new ArrayList<>();
        buyersBidList.add(buyersBid);

        doReturn(buyersBidList).when(queryDispatcher).send(isA(FindBidsByProductIdQuery.class));
        doReturn(buyersBidList).when(queryHandler).handle(isA(FindBidsByProductIdQuery.class));
        doReturn(sellersProducts).when(sellerService).getProduct(isA(String.class));

        // when
        mockMvc.perform(MockMvcRequestBuilders.get("/e-auction/api/v1/seller/show-bids/P1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        // then
    }
}
