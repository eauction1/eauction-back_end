package com.EAuction.seller.service.controllers;

import com.EAuction.common.service.dto.BaseResponse;
import com.EAuction.seller.service.api.dto.ProductBidsLookupResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.context.TestPropertySource;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EmbeddedKafka(topics = {"BidPlacedEvent"}, partitions = 3)
@TestPropertySource(properties = {"spring.kafka.consumer.bootstrap-servers=${spring.embedded.kafka.brokers}"})
public class ProductBidsControllerIntegrationTest {

    @Autowired
    TestRestTemplate testRestTemplate;

    @Test
    void getBidsByProductId() {

        //given

        String resourceUrl =
                "/e-auction/api/v1/seller/show-bids/P1";
        //when
        ResponseEntity<ProductBidsLookupResponse> responseEntity = testRestTemplate.getForEntity(resourceUrl, ProductBidsLookupResponse.class);

        //then
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody().getSellersProducts().getEmail(), "Thomas@gmail.com");
    }

    @Test
    void getBidsByProductId_5XX() {

        //given

        String resourceUrl =
                "/e-auction/api/v1/seller/show-bids/R1";
        //when
        ResponseEntity<ProductBidsLookupResponse> responseEntity = testRestTemplate.getForEntity(resourceUrl, ProductBidsLookupResponse.class);

        //then
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getStatusCode());
    }
}
