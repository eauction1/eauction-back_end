package com.EAuction.seller.service.controllers;

import com.EAuction.common.service.dto.BaseResponse;
import com.EAuction.common.service.dto.ProductCategory;
import com.EAuction.seller.service.domain.SellersProducts;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.context.TestPropertySource;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SellerControllerIntegrationTest {

    @Autowired
    TestRestTemplate testRestTemplate;

    @Test
    void addProduct() {

        //given
        // Always build new object to test this method
        SellersProducts sellersProducts = SellersProducts.builder()
                .productId("S3")
                .productName("Air compass")
                .shortDescription("International symposium at Canada")
                .detailDescription("Fixed on top of the other so it could still move and feel the wind")
                .productCategory(ProductCategory.SCULPTOR)
                .startPrice(5000.0)
                .bidEndDate("2022-12-06")
                .firstName("Nicholas")
                .lastName("Runolfsdottir")
                .address("Ellsworth Summit")
                .city("Aliyaview")
                .state("UK")
                .pin(12255)
                .phone("1115556666")
                .email("Nicholas@gmail.com")
                .build();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("content-type", MediaType.APPLICATION_JSON.toString());

        HttpEntity<SellersProducts> request = new HttpEntity<>(sellersProducts, httpHeaders);


        //when
        ResponseEntity<SellersProducts> responseEntity = testRestTemplate.exchange("/e-auction/api/v1/seller/add-product", HttpMethod.POST, request, SellersProducts.class);

        //then
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
    }

    @Test
    void addProduct_4XX() {

        // Product name size must be between 5 and 30

        //given
        SellersProducts sellersProducts = SellersProducts.builder()
                .productId("S3")
                .productName("Ai") // Product name size must be between 5 and 30
                .shortDescription("International symposium at Canada")
                .detailDescription("Fixed on top of the other so it could still move and feel the wind")
                .productCategory(ProductCategory.SCULPTOR)
                .startPrice(5000.0)
                .bidEndDate("2022-12-06")
                .firstName("Nicholas")
                .lastName("Runolfsdottir")
                .address("Ellsworth Summit")
                .city("Aliyaview")
                .state("UK")
                .pin(12255)
                .phone("1115556666")
                .email("Nicholas@gmail.com")
                .build();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("content-type", MediaType.APPLICATION_JSON.toString());

        HttpEntity<SellersProducts> request = new HttpEntity<>(sellersProducts, httpHeaders);


        //when
        ResponseEntity<BaseResponse> responseEntity = testRestTemplate.exchange("/e-auction/api/v1/seller/add-product", HttpMethod.POST, request, BaseResponse.class);

        //then
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
    }

    @Test
    void addProduct_5XX() {

        // Bid end date should be future date

        //given
        SellersProducts sellersProducts = SellersProducts.builder()
                .productId("S3")
                .productName("Air compass")
                .shortDescription("International symposium at Canada")
                .detailDescription("Fixed on top of the other so it could still move and feel the wind")
                .productCategory(ProductCategory.SCULPTOR)
                .startPrice(5000.0)
                .bidEndDate("2022-11-06") // Bid end date should be future date
                .firstName("Nicholas")
                .lastName("Runolfsdottir")
                .address("Ellsworth Summit")
                .city("Aliyaview")
                .state("UK")
                .pin(12255)
                .phone("1115556666")
                .email("Nicholas@gmail.com")
                .build();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("content-type", MediaType.APPLICATION_JSON.toString());

        HttpEntity<SellersProducts> request = new HttpEntity<>(sellersProducts, httpHeaders);

        String expectedErrorMessage = "BaseResponse(message=Error while processing request to add product - com.EAuction.cqrs.core.exceptions.FutureDateException: Bid end date should be future date!.)";

        //when
        ResponseEntity<BaseResponse> responseEntity = testRestTemplate.exchange("/e-auction/api/v1/seller/add-product", HttpMethod.POST, request, BaseResponse.class);

        //then
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getStatusCode());
//        assertEquals(expectedErrorMessage, responseEntity.getBody());
    }

    @Test
    void deleteProduct() {

        // Add new product without bids

        //given
        String resourceUrl = "/e-auction/api/v1/seller/delete/O1";


        //when
        ResponseEntity<String> responseEntity = testRestTemplate.exchange(resourceUrl, HttpMethod.DELETE, HttpEntity.EMPTY, String.class);

        //then
//        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    void deleteProduct_5XX() {

        // Product is tried to be deleted after bid end date

        //given
        String resourceUrl = "/e-auction/api/v1/seller/delete/O2";


        //when
        ResponseEntity<BaseResponse> responseEntity = testRestTemplate.exchange(resourceUrl, HttpMethod.DELETE, HttpEntity.EMPTY, BaseResponse.class);

        //then
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getStatusCode());
    }

    @Test
    void deleteProduct_5XX_AtLeastOneBidPresentException() {

        // Product is tried to be deleted when at least one bid is already placed

        //given
        String resourceUrl = "/e-auction/api/v1/seller/delete/S2";


        //when
        ResponseEntity<BaseResponse> responseEntity = testRestTemplate.exchange(resourceUrl, HttpMethod.DELETE, HttpEntity.EMPTY, BaseResponse.class);

        //then
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getStatusCode());
    }


}
