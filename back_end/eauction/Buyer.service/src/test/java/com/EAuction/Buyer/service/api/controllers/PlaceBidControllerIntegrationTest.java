package com.EAuction.Buyer.service.api.controllers;

import com.EAuction.Buyer.service.api.commands.PlaceBidCommand;
import com.EAuction.common.service.dto.BaseResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.context.TestPropertySource;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EmbeddedKafka(topics = {"BidPlacedEvent"}, partitions = 3)
@TestPropertySource(properties = {"spring.kafka.producer.bootstrap-servers=${spring.embedded.kafka.brokers}"})
public class PlaceBidControllerIntegrationTest {

    @Autowired
    TestRestTemplate testRestTemplate;

    @Test
    void placeBid() {

        //given
        // Always build new object to test this method
        PlaceBidCommand placeBidCommand = PlaceBidCommand.builder()
                .firstName("Kurtis")
                .lastName("Weissnat")
                .address("Rex Trail")
                .city("Howemouth")
                .state("Greece")
                .pin(12213)
                .phone("1112223335")
                .email("Kurtis@gmail.com")
                .productId("S1")
                .bidAmount(700.0)
                .build();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("content-type", MediaType.APPLICATION_JSON.toString());

        HttpEntity<PlaceBidCommand> request = new HttpEntity<>(placeBidCommand, httpHeaders);


        //when
        ResponseEntity<BaseResponse> responseEntity = testRestTemplate.exchange("/e-auction/api/v1/buyer/place-bid", HttpMethod.POST, request, BaseResponse.class);

        //then
//        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
    }

    @Test
    void placeBid_5XX() {

        //given
        PlaceBidCommand placeBidCommand = PlaceBidCommand.builder()
                .firstName("Dennis")
                .lastName("Schulist")
                .address("Norberto Crossing")
                .city("Christy")
                .state("Spain")
                .pin(12212)
                .phone("1112223333")
                .email("Dennis@gmail.com")
                .productId("S1")
                .bidAmount(500.0)
                .build();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("content-type", MediaType.APPLICATION_JSON.toString());

        HttpEntity<PlaceBidCommand> request = new HttpEntity<>(placeBidCommand, httpHeaders);


        //when
        ResponseEntity<BaseResponse> responseEntity = testRestTemplate.exchange("/e-auction/api/v1/buyer/place-bid", HttpMethod.POST, request, BaseResponse.class);

        //then
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getStatusCode());
    }

    @Test
    void placeBid_4XX() {

        //given

        PlaceBidCommand placeBidCommand = PlaceBidCommand.builder()
                .firstName("Dennis")
                .lastName("Schulist")
                .address("Norberto Crossing")
                .city("Christy")
                .state("Spain")
                .pin(12212)
                .phone("1112")
                .email("Dennis@gmail.com")
                .productId("S1")
                .bidAmount(500.0)
                .build();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("content-type", MediaType.APPLICATION_JSON.toString());

        HttpEntity<PlaceBidCommand> request = new HttpEntity<>(placeBidCommand, httpHeaders);


        //when
        ResponseEntity<BaseResponse> responseEntity = testRestTemplate.exchange("/e-auction/api/v1/buyer/place-bid", HttpMethod.POST, request, BaseResponse.class);

        //then
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
    }
}
