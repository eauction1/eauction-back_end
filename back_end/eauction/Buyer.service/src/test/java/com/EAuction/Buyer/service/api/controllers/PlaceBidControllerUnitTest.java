package com.EAuction.Buyer.service.api.controllers;

import com.EAuction.Buyer.service.api.commands.CommandHandler;
import com.EAuction.Buyer.service.api.commands.PlaceBidCommand;
import com.EAuction.Buyer.service.api.dto.SellersProductsResponse;
import com.EAuction.Buyer.service.utils.BuyerClient;
import com.EAuction.common.service.dto.ProductCategory;
import com.EAuction.cqrs.core.infrastructure.CommandDispatcher;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(PlaceBidController.class)
@AutoConfigureMockMvc
public class PlaceBidControllerUnitTest {

    @Autowired
    MockMvc mockMvc;

    ObjectMapper objectMapper = new ObjectMapper();

    @MockBean
    CommandDispatcher commandDispatcher;

    @MockBean
    CommandHandler commandHandler;

    @MockBean
    BuyerClient buyerClient;

    @Test
    void placeBid() throws Exception {

        // given
        // Always create new PlaceBidCommand here to test this method
        PlaceBidCommand placeBidCommand = PlaceBidCommand.builder()
                .firstName("Dennis")
                .lastName("Schulist")
                .address("Norberto Crossing")
                .city("Christy")
                .state("Spain")
                .pin(12212)
                .phone("1112223333")
                .email("Dennis@gmail.com")
                .productId("S2")
                .bidAmount(500.0)
                .build();

        SellersProductsResponse sellersProductsResponse = SellersProductsResponse.builder()
                .productId("S2")
                .productName("Gian Lorenzo Bernini")
                .shortDescription("Ecstasy of Saint Teresa, 1647–52")
                .detailDescription("Acknowledged as an originator of the High Roman Baroque style")
                .productCategory(ProductCategory.SCULPTOR)
                .startPrice(3500.0)
                .bidEndDate("2022-12-04")
                .firstName("Clementine")
                .lastName("Bauch")
                .address("Douglas Extension")
                .city("McKenziehaven")
                .state("Canada")
                .pin(12244)
                .phone("1115556665")
                .email("Clementine@gmail.com")
                .build();

        String json = objectMapper.writeValueAsString(placeBidCommand);
        doNothing().when(commandDispatcher).send(isA(PlaceBidCommand.class));
        doNothing().when(commandHandler).handle(isA(PlaceBidCommand.class));
        doReturn(sellersProductsResponse).when(buyerClient).getProduct(isA(String.class));

        // when
        mockMvc.perform(post("/e-auction/api/v1/buyer/place-bid")
                        .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        // then

    }

    @Test
    void placeBid_4XX() throws Exception {

        // given
        PlaceBidCommand placeBidCommand = PlaceBidCommand.builder()
                .firstName("Dennis")
                .lastName("Schulist")
                .address("Norberto Crossing")
                .city("Christy")
                .state("Spain")
                .pin(12212)
                .phone("1112223333")
                .email("Dennisgmail.com") // Not valid email
                .productId("S2")
                .bidAmount(500.0)
                .build();

        SellersProductsResponse sellersProductsResponse = SellersProductsResponse.builder()
                .productId("S2")
                .productName("Gian Lorenzo Bernini")
                .shortDescription("Ecstasy of Saint Teresa, 1647–52")
                .detailDescription("Acknowledged as an originator of the High Roman Baroque style")
                .productCategory(ProductCategory.SCULPTOR)
                .startPrice(3500.0)
                .bidEndDate("2022-12-04")
                .firstName("Clementine")
                .lastName("Bauch")
                .address("Douglas Extension")
                .city("McKenziehaven")
                .state("Canada")
                .pin(12244)
                .phone("1115556665")
                .email("Clementine@gmail.com")
                .build();

        String json = objectMapper.writeValueAsString(placeBidCommand);
        doNothing().when(commandDispatcher).send(isA(PlaceBidCommand.class));
        doNothing().when(commandHandler).handle(isA(PlaceBidCommand.class));
        doReturn(sellersProductsResponse).when(buyerClient).getProduct(isA(String.class));

        // when
        mockMvc.perform(post("/e-auction/api/v1/buyer/place-bid")
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());

        // then

    }

    @Test
    void placeBid_5XX() throws Exception {

        // Placing bid on product after bid end date

        // given
        PlaceBidCommand placeBidCommand = PlaceBidCommand.builder()
                .firstName("Dennis")
                .lastName("Schulist")
                .address("Norberto Crossing")
                .city("Christy")
                .state("Spain")
                .pin(12212)
                .phone("1112223333")
                .email("Dennis@gmail.com")
                .productId("S2")
                .bidAmount(500.0)
                .build();

        SellersProductsResponse sellersProductsResponse = SellersProductsResponse.builder()
                .productId("S2")
                .productName("Gian Lorenzo Bernini")
                .shortDescription("Ecstasy of Saint Teresa, 1647–52")
                .detailDescription("Acknowledged as an originator of the High Roman Baroque style")
                .productCategory(ProductCategory.SCULPTOR)
                .startPrice(3500.0)
                .bidEndDate("2022-11-04") // Placing bid on product after bid end date
                .firstName("Clementine")
                .lastName("Bauch")
                .address("Douglas Extension")
                .city("McKenziehaven")
                .state("Canada")
                .pin(12244)
                .phone("1115556665")
                .email("Clementine@gmail.com")
                .build();

        String json = objectMapper.writeValueAsString(placeBidCommand);
        doNothing().when(commandDispatcher).send(isA(PlaceBidCommand.class));
        doNothing().when(commandHandler).handle(isA(PlaceBidCommand.class));
        doReturn(sellersProductsResponse).when(buyerClient).getProduct(isA(String.class));

//        String expectedErrorMessage = "{\"message\":\"Error while processing request to place bid for product - com.EAuction.cqrs.core.exceptions.PastBidEndDateException: Placing bid on product after bid end date!\"}";

        // when
        mockMvc.perform(post("/e-auction/api/v1/buyer/place-bid")
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is5xxServerError());
//                .andExpect(content().string(expectedErrorMessage));

        // then
    }
}
