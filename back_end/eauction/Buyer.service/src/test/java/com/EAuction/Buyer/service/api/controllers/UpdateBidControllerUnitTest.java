package com.EAuction.Buyer.service.api.controllers;

import com.EAuction.Buyer.service.api.commands.CommandHandler;
import com.EAuction.Buyer.service.api.commands.PlaceBidCommand;
import com.EAuction.Buyer.service.api.commands.UpdateBidCommand;
import com.EAuction.Buyer.service.api.dto.SellersProductsResponse;
import com.EAuction.Buyer.service.utils.BuyerClient;
import com.EAuction.common.service.dto.ProductCategory;
import com.EAuction.cqrs.core.infrastructure.CommandDispatcher;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UpdateBidController.class)
@AutoConfigureMockMvc
public class UpdateBidControllerUnitTest {

    @Autowired
    MockMvc mockMvc;

    ObjectMapper objectMapper = new ObjectMapper();

    @MockBean
    CommandDispatcher commandDispatcher;

    @MockBean
    CommandHandler commandHandler;

    @MockBean
    BuyerClient buyerClient;

    @Test
    void updateBid() throws Exception {

        // given

        UpdateBidCommand updateBidCommand = UpdateBidCommand.builder()
                .updatedBidAmount(2500.0)
                .build();

        SellersProductsResponse sellersProductsResponse = SellersProductsResponse.builder()
                .productId("S2")
                .productName("Gian Lorenzo Bernini")
                .shortDescription("Ecstasy of Saint Teresa, 1647–52")
                .detailDescription("Acknowledged as an originator of the High Roman Baroque style")
                .productCategory(ProductCategory.SCULPTOR)
                .startPrice(3500.0)
                .bidEndDate("2022-12-04")
                .firstName("Clementine")
                .lastName("Bauch")
                .address("Douglas Extension")
                .city("McKenziehaven")
                .state("Canada")
                .pin(12244)
                .phone("1115556665")
                .email("Clementine@gmail.com")
                .build();

        String json = objectMapper.writeValueAsString(updateBidCommand);
        doNothing().when(commandDispatcher).send(isA(PlaceBidCommand.class));
        doNothing().when(commandHandler).handle(isA(PlaceBidCommand.class));
        doReturn(sellersProductsResponse).when(buyerClient).getProduct(isA(String.class));

        String resourceUrl =
                "/e-auction/api/v1/buyer/update-bid/adee5008-997f-47e8-ad86-1bfbd65e0075/S1/Dennis@gmail.com/"+(int)updateBidCommand.getUpdatedBidAmount();

        // when
        mockMvc.perform(put(resourceUrl)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        // then

    }

    @Test
    void updateBid_5XX() throws Exception {

        // Updating bid amount after bid end date

        // given
        UpdateBidCommand updateBidCommand = UpdateBidCommand.builder()
                .updatedBidAmount(2500.0)
                .build();

        SellersProductsResponse sellersProductsResponse = SellersProductsResponse.builder()
                .productId("S2")
                .productName("Gian Lorenzo Bernini")
                .shortDescription("Ecstasy of Saint Teresa, 1647–52")
                .detailDescription("Acknowledged as an originator of the High Roman Baroque style")
                .productCategory(ProductCategory.SCULPTOR)
                .startPrice(3500.0)
                .bidEndDate("2022-11-04") // Updating bid amount after bid end date
                .firstName("Clementine")
                .lastName("Bauch")
                .address("Douglas Extension")
                .city("McKenziehaven")
                .state("Canada")
                .pin(12244)
                .phone("1115556665")
                .email("Clementine@gmail.com")
                .build();

        String json = objectMapper.writeValueAsString(updateBidCommand);
        doNothing().when(commandDispatcher).send(isA(PlaceBidCommand.class));
        doNothing().when(commandHandler).handle(isA(PlaceBidCommand.class));
        doReturn(sellersProductsResponse).when(buyerClient).getProduct(isA(String.class));

        String resourceUrl =
                "/e-auction/api/v1/buyer/update-bid/adee5008-997f-47e8-ad86-1bfbd65e0075/S1/Dennis@gmail.com/"+(int)updateBidCommand.getUpdatedBidAmount();

        // when
        mockMvc.perform(put(resourceUrl)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is5xxServerError());

        // then
    }
}
