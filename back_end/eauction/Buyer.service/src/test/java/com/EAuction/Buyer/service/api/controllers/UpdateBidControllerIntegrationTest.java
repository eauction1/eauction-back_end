package com.EAuction.Buyer.service.api.controllers;

import com.EAuction.Buyer.service.api.commands.PlaceBidCommand;
import com.EAuction.Buyer.service.api.commands.UpdateBidCommand;
import com.EAuction.common.service.dto.BaseResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.context.TestPropertySource;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EmbeddedKafka(topics = {"BidUpdatedEvent"}, partitions = 3)
@TestPropertySource(properties = {"spring.kafka.producer.bootstrap-servers=${spring.embedded.kafka.brokers}"})
public class UpdateBidControllerIntegrationTest {

    @Autowired
    TestRestTemplate testRestTemplate;

    @Test
    void updateBid() {

        //given
        // Always build new object to test this method
        UpdateBidCommand updateBidCommand = UpdateBidCommand.builder()
                .updatedBidAmount(600)
                .build();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("content-type", MediaType.APPLICATION_JSON.toString());

        HttpEntity<UpdateBidCommand> request = new HttpEntity<>(updateBidCommand);
        String resourceUrl =
                "/e-auction/api/v1/buyer/update-bid/adee5008-997f-47e8-ad86-1bfbd65e0075/S1/Dennis@gmail.com/"+(int)updateBidCommand.getUpdatedBidAmount();


        //when
        ResponseEntity<BaseResponse> responseEntity = testRestTemplate.exchange(resourceUrl, HttpMethod.PUT, request, BaseResponse.class);

        //then
//        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }
}
