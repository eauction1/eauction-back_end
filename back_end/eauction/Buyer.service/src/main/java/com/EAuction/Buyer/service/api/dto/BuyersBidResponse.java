package com.EAuction.Buyer.service.api.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BuyersBidResponse {

        private String id;
        private String firstName;
        private String lastName;
        private String address;
        private String city;
        private String state;
        private int pin;
        private String phone;
        private String email;
        private String productId;
        private double bidAmount;
}

