package com.EAuction.Buyer.service.api.commands;

import com.EAuction.Buyer.service.domain.EAuctionAggregate;
import com.EAuction.cqrs.core.handlers.EventSourcingHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EAuctionCommandHandler implements CommandHandler{
    @Autowired
    private EventSourcingHandler<EAuctionAggregate> eventSourcingHandler;

    @Override
    public void handle(PlaceBidCommand command) {
        var aggregate = new EAuctionAggregate(command);
        eventSourcingHandler.save(aggregate);
    }

    @Override
    public void handle(UpdateBidCommand command) {
        var aggregate = eventSourcingHandler.getById(command.getId());
        aggregate.updateBid(command.getUpdatedBidAmount());
        eventSourcingHandler.save(aggregate);
    }
}
