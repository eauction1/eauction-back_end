package com.EAuction.Buyer.service.api.dto;

import com.EAuction.common.service.dto.BaseResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlaceBidResponse extends BaseResponse {
    private String id;

    public PlaceBidResponse(String message, String id) {
        super(message);
        this.id = id;
    }
}
