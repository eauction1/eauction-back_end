package com.EAuction.Buyer.service.api.dto;

import com.EAuction.common.service.dto.BaseResponse;
import com.EAuction.common.service.dto.ProductCategory;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class SellersProductsResponse extends BaseResponse {
    private String productId;
    private String productName;
    private String shortDescription;
    private String detailDescription;
    @Enumerated(EnumType.STRING)
    private ProductCategory productCategory;
    private double startPrice;
    @JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING)
    private String bidEndDate;
    private String firstName;
    private String lastName;
    private String address;
    private String city;
    private String state;
    private int pin;
    private String phone;
    private String email;

    public SellersProductsResponse(String message, String productId) {
        super(message);
        this.productId = productId;
    }
}
