package com.EAuction.Buyer.service.infrastructure;

import com.EAuction.Buyer.service.domain.EAuctionAggregate;
import com.EAuction.cqrs.core.domain.AggregateRoot;
import com.EAuction.cqrs.core.handlers.EventSourcingHandler;
import com.EAuction.cqrs.core.infrastructure.EventStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;

@Service
public class EAuctionEventSourcingHandler implements EventSourcingHandler<EAuctionAggregate> {
    @Autowired
    private EventStore eventStore;

    @Override
    public void save(AggregateRoot aggregate) {
        eventStore.saveEvents(aggregate.getId(), aggregate.getUncommittedChanges(), aggregate.getVersion());
        aggregate.markChangesAsCommitted();;

    }

    @Override
    public EAuctionAggregate getById(String id) {
        var aggregate = new EAuctionAggregate();
        var events = eventStore.getEvents(id);
        if(events != null && !events.isEmpty()) {
            aggregate.replayEvents(events);
            var latestVersion = events.stream().map(x -> x.getVersion()).max(Comparator.naturalOrder());
            aggregate.setVersion(latestVersion.get());
        }
        return aggregate;
    }
}
