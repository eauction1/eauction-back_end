package com.EAuction.Buyer.service.utils;

import com.EAuction.Buyer.service.api.dto.BuyersBidResponse;
import com.EAuction.Buyer.service.api.dto.SellersProductsResponse;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "Buyer-Client", url = "http://localhost:5001/e-auction/api/v1/seller")
public interface BuyerClient {

    @GetMapping("/getProduct/{productId}")
    public SellersProductsResponse getProduct(@PathVariable(value = "productId") String productId);

    @GetMapping("/getProductsBids/{email}")
    public List<BuyersBidResponse> getProductsBids(@PathVariable(value = "email") String email);
}
