package com.EAuction.Buyer.service.api.controllers;

import com.EAuction.Buyer.service.api.commands.PlaceBidCommand;
import com.EAuction.Buyer.service.api.dto.BuyersBidResponse;
import com.EAuction.Buyer.service.api.dto.PlaceBidResponse;
import com.EAuction.Buyer.service.api.dto.SellersProductsResponse;
import com.EAuction.Buyer.service.utils.BuyerClient;
import com.EAuction.common.service.dto.BaseResponse;
import com.EAuction.cqrs.core.exceptions.FutureDateException;
import com.EAuction.cqrs.core.exceptions.MoreThanOneBidsBySameBuyerException;
import com.EAuction.cqrs.core.exceptions.PastBidEndDateException;
import com.EAuction.cqrs.core.infrastructure.CommandDispatcher;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.Valid;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/e-auction/api/v1/buyer")
@Api(value = "PlaceBidController, Controller that deals with placing bid for product!")
public class PlaceBidController {
    private final Logger logger = Logger.getLogger(PlaceBidController.class.getName());

    @Autowired
    private BuyerClient buyerClient;

    @Autowired
    private CommandDispatcher commandDispatcher;


    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    LocalDateTime now = LocalDateTime.now();

    @PostMapping("/place-bid")
    @ApiOperation(value = "Place bid for product", response = BaseResponse.class, tags = "placeBid")
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Place the bid for product successfully!"),
                            @ApiResponse(code = 400, message = "Client made a bad request!"),
                            @ApiResponse(code = 500, message = "Error while processing request to place bid for product!")})
    public ResponseEntity<BaseResponse> placeBid(@Valid @RequestBody PlaceBidCommand command) {
        var id = UUID.randomUUID().toString();

        try {
            logger.log(Level.INFO, "Inside placeBid method of PlaceBidController!");

            SellersProductsResponse sellersProductsResponse = buyerClient.getProduct("S1");
            if(sellersProductsResponse.getBidEndDate().compareTo(dtf.format(now)) < 0){
                throw new PastBidEndDateException("Placing bid on product after bid end date!");
            }
            List<BuyersBidResponse> buyersBidResponsesList = buyerClient.getProductsBids(command.getEmail());
            List<BuyersBidResponse> currentByersBidList = buyersBidResponsesList.stream().filter(x -> x.getEmail().equals(command.getEmail()) && x.getProductId().equals(command.getProductId()))
                    .collect(Collectors.toList());
            if(currentByersBidList.size() > 0){
                throw new MoreThanOneBidsBySameBuyerException("More than one bids by same buyer on same product is not allowed!");
            }
            var productId = sellersProductsResponse.getProductId();
            command.setId(id);
            command.setProductId(productId);
            commandDispatcher.send(command);
            logger.log(Level.INFO, MessageFormat.format("Bid placed request completed successfully! - (0).", productId));
            return new ResponseEntity<>(new PlaceBidResponse("Bid placed request completed successfully!", id), HttpStatus.CREATED);
        } catch (IllegalStateException e) {
            logger.log(Level.WARNING, MessageFormat.format("Client made a bad request - {0}.", e.toString()));
            return new ResponseEntity<>(new BaseResponse(e.toString()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            var safeErrorMessage = MessageFormat.format("Error while processing request to place bid for product - {0}", e.toString());
            logger.log(Level.SEVERE, safeErrorMessage, e);
            return new ResponseEntity<>(new PlaceBidResponse(safeErrorMessage, id), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//    @GetMapping("/getProduct/{productId}")
//    public SellersProductsResponse getProduct(@PathVariable(value = "productId") String productId) {
//        return buyerClient.getProduct(productId);
//    }
}
