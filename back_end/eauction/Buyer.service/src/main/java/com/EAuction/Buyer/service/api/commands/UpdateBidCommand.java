package com.EAuction.Buyer.service.api.commands;

import com.EAuction.cqrs.core.commands.BaseCommand;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UpdateBidCommand extends BaseCommand {
    private double updatedBidAmount;
}
