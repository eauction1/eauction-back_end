package com.EAuction.Buyer.service.api.commands;

public interface CommandHandler {
    void handle(PlaceBidCommand command);
    void handle(UpdateBidCommand command);
}
