package com.EAuction.Buyer.service.api.controllers;

import com.EAuction.Buyer.service.api.commands.UpdateBidCommand;
import com.EAuction.Buyer.service.api.dto.PlaceBidResponse;
import com.EAuction.Buyer.service.api.dto.SellersProductsResponse;
import com.EAuction.Buyer.service.utils.BuyerClient;
import com.EAuction.common.service.dto.BaseResponse;
import com.EAuction.cqrs.core.exceptions.AggregateNotFoundException;
import com.EAuction.cqrs.core.exceptions.PastBidEndDateException;
import com.EAuction.cqrs.core.infrastructure.CommandDispatcher;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
@RequestMapping(path = "/e-auction/api/v1/buyer")
@Api(value = "UpdateBidController, Controller that deals with updating bid for product!")
public class UpdateBidController {
    private final Logger logger = Logger.getLogger(UpdateBidController.class.getName());

    @Autowired
    private CommandDispatcher commandDispatcher;

    @Autowired
    private BuyerClient buyerClient;

    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    LocalDateTime now = LocalDateTime.now();

    @PutMapping("/update-bid/{id}/{productId}/{buyerEmailId}/{newBidAmount}")
    @ApiOperation(value = "Update bid for product", response = BaseResponse.class, tags = "updateBid")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Update the bid for product successfully!"),
            @ApiResponse(code = 400, message = "Client made a bad request!"),
            @ApiResponse(code = 500, message = "Error while processing request to update bid for product!")})
    public ResponseEntity<BaseResponse> updateBid(@PathVariable(value = "id") String id,
                                                  @PathVariable(value = "productId") String productId,
                                                  @PathVariable(value = "buyerEmailId") String buyerEmailId,
                                                  @PathVariable(value = "newBidAmount") int newBidAmount,
                                                      @RequestBody UpdateBidCommand command) {
        try {
            logger.log(Level.INFO, "Inside updateBid method of UpdateBidController!");
            SellersProductsResponse sellersProductsResponse = buyerClient.getProduct(productId);
            if(sellersProductsResponse.getBidEndDate().compareTo(dtf.format(now)) < 0){
                throw new PastBidEndDateException("Updating bid amount after bid end date!");
            }
            command.setId(id);
            commandDispatcher.send(command);
            logger.log(Level.INFO, MessageFormat.format("Bid update request completed successfully! - (0).", productId));
            return new ResponseEntity<>(new BaseResponse("Bid update request completed successfully!"), HttpStatus.OK);


        } catch (IllegalStateException | AggregateNotFoundException e) {
            logger.log(Level.WARNING, MessageFormat.format("Client made a bad request - {0}.", e.toString()));
            return new ResponseEntity<>(new BaseResponse(e.toString()), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            var safeErrorMessage = MessageFormat.format("Error while processing request to update bid for product for id - {0}.", id);
            logger.log(Level.SEVERE, safeErrorMessage, e);
            return new ResponseEntity<>(new BaseResponse(safeErrorMessage), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
