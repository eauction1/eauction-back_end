package com.EAuction.Buyer.service.api.commands;

import com.EAuction.cqrs.core.commands.BaseCommand;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlaceBidCommand extends BaseCommand {
    @NotBlank(message = "First Name should be present, Please check!")
    @Size(min = 5, max = 30, message = "First name size must be between 5 and 30")
    private String firstName;
    @NotBlank(message = "Last Name should be present, Please check!")
    @Size(min = 3, max = 25, message = "Last name size must be between 3 and 25")
    private String lastName;
    private String address;
    private String city;
    private String state;
    private int pin;
    @NotNull(message = "Phone number should be present, Please check!")
    @Pattern(regexp = "\\d{10}", message = "Phone number is not in format, Please add 10 digit numeric Phone number!")
    private String phone;
    @NotNull(message = "Email should be present, Please check!")
    @Email(message = "Email is not in format, Please add single @")
    private String email;
    private String productId;
    private double bidAmount;
}
