package com.EAuction.Buyer.service.domain;

import com.EAuction.Buyer.service.api.commands.PlaceBidCommand;
import com.EAuction.Buyer.service.api.dto.SellersProductsResponse;
import com.EAuction.Buyer.service.utils.BuyerClient;
import com.EAuction.common.service.events.BidPlacedEvent;
import com.EAuction.common.service.events.BidUpdatedEvent;
import com.EAuction.cqrs.core.domain.AggregateRoot;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@NoArgsConstructor
@Component
public class EAuctionAggregate extends AggregateRoot {
    private double amount;
    public EAuctionAggregate(PlaceBidCommand command) {

        raiseEvent(BidPlacedEvent.builder()
                .id(command.getId())
                .firstName(command.getFirstName())
                .lastName(command.getLastName())
                .address(command.getAddress())
                .city(command.getCity())
                .state(command.getState())
                .pin(command.getPin())
                .phone(command.getPhone())
                .email(command.getEmail())
                .productId(command.getProductId())
                .bidAmount(command.getBidAmount())
                .build());
    }

    public void apply(BidPlacedEvent event) {
        this.id = event.getId();
        this.amount = event.getBidAmount();
    }

    public void updateBid(double updatedBidAmount) {

        raiseEvent(BidUpdatedEvent.builder()
                .id(this.id)
                .updatedBidAmount(updatedBidAmount)
                .build());
    }

    public void apply(BidUpdatedEvent event) {
        this.id = event.getId();
        this.amount = event.getUpdatedBidAmount();
    }
}


