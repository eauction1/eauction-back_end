package com.EAuction.api.gateway.controllers;

import com.EAuction.api.gateway.dto.JWTRequest;
import com.EAuction.api.gateway.dto.JWTResponse;
import com.EAuction.api.gateway.service.UserService;
import com.EAuction.api.gateway.util.JWTUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JWTController {

    @Autowired
    private JWTUtility jwtUtility;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserService userService;

    @CrossOrigin(origins = "*")
    @PostMapping("/authenticate")
    public ResponseEntity<JWTResponse> generateToken(@RequestBody JWTRequest jwtRequest)
              throws AuthenticationException {
            final Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            jwtRequest.getUserName(),
                            jwtRequest.getPassword()
                    )
            );
            SecurityContextHolder.getContext().setAuthentication(authentication);
            final UserDetails userDetails = userService.loadUserByUsername(jwtRequest.getUserName());
            final String token = jwtUtility.generateToken(userDetails);
            return ResponseEntity.ok(new JWTResponse(token));
        }
}
