package com.EAuction.api.gateway.controllers;

import org.springframework.web.bind.annotation.*;

@RestController
public class APIGatewayFallBackContoller {

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @RequestMapping("/sellerServiceFallBack")
    public String sellerServiceFallBackMethod() {
        return "Seller Service is taking longer time than expected. Please try again later!";
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @RequestMapping("/buyerServiceFallBack")
    public String buyerServiceFallBackMethod() {
        return "Buyer Service is taking longer time than expected. Please try again later!";
    }
}
