package com.EAuction.common.service.dto;

public enum ProductCategory {
    PAINTING,
    SCULPTOR,
    ORNAMENT
}
