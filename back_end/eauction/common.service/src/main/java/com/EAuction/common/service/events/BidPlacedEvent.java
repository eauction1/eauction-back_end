package com.EAuction.common.service.events;

import com.EAuction.cqrs.core.events.BaseEvent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class BidPlacedEvent extends BaseEvent {
    private String firstName;
    private String lastName;
    private String address;
    private String city;
    private String state;
    private int pin;
    private String phone;
    private String email;
    private String productId;
    private double bidAmount;
}
