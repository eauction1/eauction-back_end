package com.EAuction.common.service.events;

import com.EAuction.cqrs.core.events.BaseEvent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class BidUpdatedEvent extends BaseEvent {
    private double updatedBidAmount;
}
